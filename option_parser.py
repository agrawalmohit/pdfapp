# -*- coding: utf-8 -*-
"""
Provides a custom option parser and options global
"""
import sys
from tornado.log import logging
from tornado.options import OptionParser, Error


logger = logging.getLogger()


class PdfappOptionParser(OptionParser):
    def parse_command_line(self, args=None, final=True):
        if args is None:
            args = sys.argv
        remaining = []
        for i in range(1, len(args)):
            # All things after the last option are command line arguments
            if not args[i].startswith("-"):
                remaining = args[i:]
                break
            if args[i] == "--":
                remaining = args[i + 1:]
                break
            arg = args[i].lstrip("-")
            name, equals, value = arg.partition("=")
            name = name.replace('-', '_')
            if not name in self._options:
                logger.warn("Unrecognized command line option: {:}".format(name))
                continue
            option = self._options[name]
            if not equals:
                if option.type == bool:
                    value = "true"
                else:
                    raise Error('Option %r requires a value' % name)
            option.parse(value)
        if final:
            self.run_parse_callbacks()
        return remaining

options = PdfappOptionParser()
