app.factory('$ocket', ['Notify', function(Notify){

    console.log("Service : $ocket");

    var url = window.location.origin + '/ws';
    var sock = new SockJS(url);


    sock.onopen = function() {
        console.log("Websocket : Opened connection")
    }

    sock.onmessage = function(e) {
       var response = e.data;
       console.log("Websocket(incoming message) : ");
       console.log(response)
       Notify.info(response.message)

    };

    sock.onclose = function() {
       console.log('Websocket : Closed connection');
    };

    return {
        send : function(message){
            console.log("Websocket(sending message) : ", message)
            sock.send(message);
        }
    }

}])
