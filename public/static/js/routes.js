app.config( function ( $routeProvider,$locationProvider ) {
  $routeProvider
  	.when( '/', {
  		templateUrl : 'public/views/job.html',
  		controller : 'JobCtrl'
  	})
    .otherwise( { redirectTo: '/' } );

     $locationProvider.html5Mode(true);
});

