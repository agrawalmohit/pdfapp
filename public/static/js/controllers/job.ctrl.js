app.controller('JobCtrl', ['$scope', '$http', '$ocket', function($scope, $http, $ocket){
    
    $scope.jobstatus = 'Ready';

    $scope.start = function () {
        $ocket.send('start')
    };

    $scope.next = function () {
        $ocket.send('next')
    };
    
    $scope.stop = function () {
        $ocket.send('stop')
    };

}])
        