"""
settings.py
"""
import os
from tornado.options import Error
from tornado.log import logging
from option_parser import options


logger = logging.getLogger()


try:
    options.define("port", default=9000, help="port", type=int)
    options.define("mongodb_host",default='localhost', help='Mongo host', type=str)
    options.define("mongodb_name",default='pdfapp', help='Mongo database name', type=str)
    options.define("mongodb_port", default=27017, help="Mongo port number", type=int)
    options.define("init_db",default=1, help='Initialize database', type=int)
    options.define("templates_path",
           default=os.path.join(os.path.dirname(os.path.realpath(__file__)),'templates'),
           help='templates directory', type=str)
    options.define("public_path",
           default=os.path.join(os.path.dirname(os.path.realpath(__file__)),'public/'),
           help='public path', type=str)
    options.define("static_path",
           default=os.path.join(os.path.dirname(os.path.realpath(__file__)),'public/static'),
           help='static path', type=str)
    options.define("autoreload", default=True, type=bool)
    options.define("debug", default=True, type=bool)

except Error as e:
    logger.warn(e)
