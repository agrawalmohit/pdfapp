import os
import pdfquery
from pdfquery.cache import FileCache
import math
import time
import pdb
from app.pdfie import extract_info 

def get_files():
	for (dirpath, dirnames, files) in os.walk(os.path.join(os.path.dirname(__file__),'../pdfs')):
		return files

def extract_data_from_pdf(callback):
	for (dirpath, dirnames, files) in os.walk(os.path.join(os.path.dirname(__file__),'../pdfs')):
		for file in files:
		
			pdf = pdfquery.PDFQuery(os.path.join(dirpath, file), parse_tree_cacher=FileCache("/tmp/"))
			pdf.load()
			
			rounding_section = pdf.pq('LTTextLineHorizontal:contains("Rounding")').parent()
			rounding_box = (math.floor(float(rounding_section.attr('x0'))), math.floor(float(rounding_section.attr('y0'))), math.ceil(float(rounding_section.attr('x1'))), math.ceil(float(rounding_section.attr('y1'))))
			if(0 in rounding_box):
				pdfdata = {
					'text' : pdf.pq('LTTextLineHorizontal:contains("Rounding")').parent().text()
				}
			else:
				pdfdata = pdf.extract([
					('with_formatter', 'text'),
					('text' , 'LTTextBoxHorizontal:in_bbox("%s, %s, %s, %s"):contains("Delivery"):contains("Return"):contains("Amount")' % rounding_box)
				])
			if pdfdata['text'] == '':
				pdfdata['text'] = rounding_section.text()

			pdfdata['file'] = file
			pdfdata['message'] = "Parsed " + file
			
			for i in range(1,5):
				time.sleep(1) # delays for 1 second
			
			# Extract the information from the extracted text
			pdfdata['info'] = extract_info(pdfdata['text'])
			callback(pdfdata)
		
