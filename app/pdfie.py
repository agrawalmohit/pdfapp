
import nltk
import re
from nltk import sent_tokenize
from nltk import word_tokenize

CUR = ['USD', 'EUR'] # can be imported from files


def extract_info(doc): # need to be refactorized
    da = {} # delivery amount
    ra = {} # return amount
    #print (type(doc))

    found_cur = 0
    for i in CUR:
        if re.search(i, doc)!= None:
            found_cur = 1
            da['currency']= re.search(i, doc).group()
            ra['currency']= re.search(i, doc).group()
        else:
            pass
        if found_cur == 1:
            break

    
    sentences = sent_tokenize(doc) #
    # sentences = [word_tokenize(sent) for sent in sentences]
    # print (sentences[2])
    for sent in sentences:
        # A and B -> C and D amount
        if re.search('[Dd]elivery [Aa]mount and the [Rr]eturn [Aa]mount.+of [\w].*?(?=\d)\d+[,]\d+', sent) != None:
            delivery_n_return = re.search('[Dd]elivery [Aa]mount and the [Rr]eturn [Aa]mount.+of [\w].*?(?=\d)\d+[,]\d+', sent).group()
            if delivery_n_return.find('up')!= -1 and delivery_n_return.find('down')!= -1:
                if delivery_n_return.find('up') < delivery_n_return.find('down'):
                    da['rounding'] = 'up'
                    ra['rounding'] = 'down'
                else:
                    da['rounding'] = 'down'
                    ra['rounding'] = 'up'
            else:
                pass
            amount = re.search('\d+[,]\d+', delivery_n_return).group()
            da['amount'] = amount
            ra['amount'] = amount
        
        # 
        elif re.search('[Dd]elivery [Aa]mount \(VM\) [\w].*?(?=\d)\d+[,]\d+', sent) != None:
            delivery = re.search('[Dd]elivery [Aa]mount \(VM\) [\w].*?(?=\d)\d+[,]\d+', sent).group()
            if 'up' in delivery:
                da['rounding'] = 'up'
            elif 'down' in delivery:
                da['rounding'] = 'down'
            else: pass
            amount = re.search('\d+[,]\d+', delivery).group()
            da['amount'] = amount

        elif re.search('[Rr]eturn [Aa]mount \(VM\) [\w].*?(?=\d)\d+[,]\d+', sent) != None:
            delivery = re.search('[Rr]eturn [Aa]mount \(VM\) [\w].*?(?=\d)\d+[,]\d+', sent).group()
            if 'up' in delivery:
                ra['rounding'] = 'up'
            elif 'down' in delivery:
                ra['rounding'] = 'down'
            else: pass
            amount = re.search('\d+[,]\d+', delivery).group()
            ra['amount'] = amount
            
    return ({
    	'delivery amount' : da,
    	'return amount' : ra
    })