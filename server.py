import sys,os
import tornado
from tornado import gen
from tornado import ioloop
from tornado.web import asynchronous, RequestHandler, Application
import tornado.websocket
from sockjs.tornado import SockJSConnection, SockJSRouter

from app.pdfreader import get_files, extract_data_from_pdf
import multiprocessing
from settings import options
import threading
import pdb


sys.path.append(os.getcwd())
files = []

for (dirpath, dirnames, filenames) in os.walk(os.path.join(os.path.dirname(__file__),'pdfs')):
    files=filenames
    
num=len(files)

def setup(event_):
    global event
    event = event_

event = multiprocessing.Event() # initially unset, so workers will be paused at first
pool = multiprocessing.Pool(num, setup, (event,))

start_msg = 'Parsing PDFs...'






class MainHandler(RequestHandler):
    def get(self):
       self.render('app/templates/index.html')

clients = set()


class SockHandler(SockJSConnection):

    def on_open(self, request):
        clients.add(self)


    def on_message(self, message):
       
        if (message == 'start'):
            print("Websocket message : ", message)
            self.send({'message' : start_msg})
            
            thr = threading.Thread(target=extract_data_from_pdf, args=(self.on_result,)).start()
    
        if (message == 'next'):
            print("Websocket message : ", message)
            

    def on_result(self, response):
        self.send(response)
        #thr.join()

    def on_error(self, e):
        print(e)

    def on_close(self):
        print("WebSocket closed")
        clients.remove(self)
        self.close()

SocketRouter = SockJSRouter(SockHandler, '/ws')
application = Application(handlers= SocketRouter.urls + [
    (r'/', MainHandler),
    (r'/ws', SockHandler),
    (r'/public/(.*)', tornado.web.StaticFileHandler, {'path': options.public_path})],
    static_path= options.static_path
    )


if __name__ == "__main__":
    application.listen(options.port)
    print ('Server running on http://localhost:{:}'.format(options.port))
    ioloop.IOLoop.instance().start()
